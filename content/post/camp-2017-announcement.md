---
categories:
- Summer Camp
date: "2017-03-20"
tags: [] # This [] is required to indicate that tags is an empty list. To add tags, follow the normal list syntax using hyphens and newlines.
title: "Announcing SLOBotics' 2017 Summer Camp"
---

We are very excited to announce that SLOBotics will be holding a summer camp for 7 to 12 year olds during the week of June 12 to 16. There will be two sessions, morning and afternoon. Please help spread the word!

{{< gdrive src="0B1BGAcGEP-wGeE81ODZyNkFXb2M" width="100%" height="700" >}}
