---
categories: []
date: "2018-01-26"
tags: [] # This [] is required to indicate that tags is an empty list. To add tags, follow the normal list syntax using hyphens and newlines.
title: "2018 Central Coast VEX Tournament Livestream"
---

This year, SLOBotics hosted the annual Central Coast VEX Tournament. Much thanks to Cal Poly San Luis Obispo for providing equipment and a space for the competition!

{{< youtube q-YLL9tl1wQ >}}
