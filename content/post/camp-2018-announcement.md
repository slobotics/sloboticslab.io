---
categories:
- Summer Camp
date: "2018-03-20"
tags: [] # This [] is required to indicate that tags is an empty list. To add tags, follow the normal list syntax using hyphens and newlines.
title: "Announcing SLOBotics' 2018 Summer Camp"
---

We are very excited to announce that SLOBotics will be holding a summer camp for 7 to 14 year olds during the weeks of June 11 and June 18. Each session will be one week long and each week will have a morning and afternoon session. Please help spread the word!

{{< gdrive src="1908cwP7hOVGg0QC3WhksG3QfTBWZLv3J" width="100%" height="700" >}}
