# Configuring a development environment
*   Use git 2.10 or higher.<sup>1</sup>
*   Install the latest version of Hugo. Instructions [here](https://gohugo.io/getting-started/installing/#binary-cross-platform).
*   Clone the repository using `git clone --recursive git@gitlab.com:slobotics/slobotics.gitlab.io.git`. The recursive flag is crucial in instructing git to clone the theme submodule.

<sup>1</sup>: This has been required since 2eeab939. See [this Stack Oveflow post](https://stackoverflow.com/questions/29435156/what-will-text-auto-eol-lf-in-gitattributes-do) for more information.

# Git basics
New to git? Please read at least 1.3-2.5 (inclusive) of [this introduction](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics).

# Understanding Hugo
Hugo is a **static site generator**. It takes in a variety of files, including content (like blog posts), images, and themes, and spits out a proper combination into a deploy-ready website. It allows one to design a website in a human-readable way rather than having to deal directly with HTML, CSS, and JS.

As an example of how useful it is to use a static site generator like Hugo, take a look at the file [content/post/camp-2017-announcement.md](content/post/camp-2017-announcement.md). (You will want to view the raw text file, not the rendered version that GitLab displays by default.) Notice that there are two parts to this file:

1.  The post's YAML front matter, situated between `---` and `---`. This block is used to give Hugo information about the post, such as it's title, date, and any taxonomies (categories and tags for our purposes) it should fall under.
2.  The rest of the file, formatted in Markdown. Markdown is a lightweight markup language that allows one to write human-readable text files rather than having to write in HTML directly. If you are not familiar with Markdown, I suggest briefly checking out a guide to learn the basic syntax.

In the body (Markdown) section of this file ([content/post/camp-2017-announcement.md](content/post/camp-2017-announcement.md)) in particular, there is something special that is not normal Markdown, but specific to Hugo. A **shortcode** called `gdrive` is used. Often times, one has to add HTML to the body section of a post in order to include some feature, such as an embedded YouTube player or, in our case, an embedded Google Drive preview. It's often unfavorable to include raw HTML in the body for a few reasons:

1.  Hugo and Markdown are about human-readability. Markdown files with raw HTML do not align with this cause, and it can often be unclear at a glance what the HTML in such files is doing.
2.  Including raw HTML can be repetitive. For example, if you need YouTube embedded players in two different posts, pointing to different videos, it would be silly to copy and paste the HTML source for the entire player into both Markdown files.

**Shortcodes** aim to solve this issue. In our file, for example, we use the `gdrive` shortcode with the line `{{< gdrive src="0B1BGAcGEP-wGeE81ODZyNkFXb2M" width="100%" height="700" >}}`. The `gdrive` shortcode is defined elsewhere (namely, in the file [layouts/shortcodes/gdrive.html](layouts/shortcodes/gdrive.html)), and we simply reference it here. Looking in the [layouts/shortcodes/gdrive.html](layouts/shortcodes/gdrive.html) file, we can see that this shortcode isn't all that complicated: it corresponds to only one line of HTML: `<iframe src="https://drive.google.com/file/d/{{ .Get "src" }}/preview" width={{ .Get "width" }} height={{ .Get "height" }}></iframe>`. This line isn't actually strictly HTML, as it contains some Hugo-specific syntax for parameters. Anything between `{{` and `}}` will be replaced with the appropriate parameter in the final HTML post that Hugo creates. When we invoke the `gdrive` shortcode, we provided values for these parameters: namely `src`, `width`, and `height`. In effect, invoking the shortcode `{{< gdrive src="0B1BGAcGEP-wGeE81ODZyNkFXb2M" width="100%" height="700" >}}` is equivalent to placing the HTML `<iframe src="https://drive.google.com/file/d/0B1BGAcGEP-wGeE81ODZyNkFXb2M/preview" width={{100%}} height={{700}}></iframe>` directly in the file [content/post/camp-2017-announcement.md](content/post/camp-2017-announcement.md).

There are plenty of other neat things Hugo has to offer, but shortcodes are focused on here as we make use of them frequently in this repository.

So: We saw that [content/post/camp-2017-announcement.md](content/post/camp-2017-announcement.md) has two parts, is only 12 lines long. Now, let's compare this to the corresponding post Hugo generates, visible at [www.slobotics.club/post/camp-2017-announcement/](http://www.slobotics.club/post/camp-2017-announcement/). This is clearly different---it has a background image, links to other pages, and more. Hugo has automatically put the content of our post into a pre-configured post page with these other nice elements. Hugo also has added our post to the [Summer Camp category page](http://www.slobotics.club/categories/summer-camp/) and more, linking various source files with our human-readable Markdown post to create a proper web-ready HTML post. Because we're using Hugo, creating a new post is as simple as making a copy of the post file with a new name and changing its contents.

## The config.toml file
One important file that has gone unmentioned thus far is the [config.toml](config.toml) file. This file specifies much of the general information about the website and controls the content of the index (home) page.

## Other folders: static/ and themes/
Thus far, we have discussed the content/ folder (content/post/) and the layouts/ folder (layouts/shortcodes/), but not the static/ or themes/ folders.

### static/
static/ is where one places things that hugo is not meant to touch. When the website is built, the contents of this folder are kept as-is, and included in the final build of the website. The folder, for example, currently contains the favicon, the background and SLOHS logo images, and the homepages images of the robots.

Do note that the fact that you can place images here does not necessarily mean you should. Because our website is being hosted for free by GitLab.com, the size of the hosted website is limited to 100MB. This is why, for example, Imgur is being used to host the images featured in [content/post/camp-2017-photos.md](content/post/camp-2017-photos.md).

### themes/
themes/ is where much of the magic happens. At the time of writing, the currently used theme is Strata. SLOBotics maintains a fork of [digitalcraftsman/hugo-strata-theme](https://github.com/digitalcraftsman/hugo-strata-theme) on GitHub: [slobotics/hugo-strata-theme](https://github.com/slobotics/hugo-strata-theme).

There is generally not a need to modify the theme, as the major modifications required were done when the website was first brought online. However, if you do need to modify the theme, refer to the below section "Updating the theme submodule"

# Understanding the build process and GitLab-integrated workflow
When you are editing the website on your local machine, you will likely want to see a live preview of the site. For this, use `hugo server` and open a web browser to the given address.

GitLab.com hosts the SLOBotics website for free via GitLab Pages. The website can actually be seen at [slobotics.gitlab.io](http://slobotics.gitlab.io/), though it may appear broken here as it is configured to be viewed from [slobotics.club](http://slobotics.club).

GitLab CI is configured (see the config file [.gitlab-ci.yml](.gitlab-ci.yml)) to automatically build the website using Hugo and publish said build each time one pushes to the repository's master branch (usually with `git push origin master`). On  the GitLab web interface, the results of each build are displayed next to the commit the build went off of. The pass/fail status of the most recent pipeline (build) can also be seen on the rendered [README.md](README.md) file. A more detailed pipeline history can be seen [here](https://gitlab.com/slobotics/slobotics.gitlab.io/pipelines). You will generally be okay as long as the pipelines are not failing and the website looks as it should.

# Guides for common tasks
## Creating a post
With Hugo, each post is a Markdown file in [content/post/](content/post/). The easiest way to create a new post is to simply make a copy of an existing post and modify it. Do not forget to modify the YAML front matter as well (date, title, categories, and tags) as is appropriate. You should keep the new file syntactically similar (in filename and in contents) to those posts that exist already.

When you have previewed the new post locally (`hugo server`) and are ready to push changes to the origin (GitLab.com), you'll want to run a few git commands:

```bash
# verify that there are not any changes staged for commit that should not be
git status
# stage the new file for commit
git add content/post/
# verify that the appropriate changes, and no others, are staged for commit
git status
git diff --staged
# commit the changes
# for help with writing a commit message, consider reading through some past ones: git log --all --decorate --oneline --graph
git commit -m "Your commit message here"
# push the new commit (on the master branch) to the origin/master branch (GitLab.com)
git push origin master
```

## Updating the Hugo version used by CI
Though it is generally not critical to do, it is a good idea to use a recent version of Hugo to build the website. The following steps accomplish this.

1.  Visit [the releases page on Hugo's GitHub](https://github.com/gohugoio/hugo/releases/latest) and note the latest version number.
2.  Update the `HUGO_VERSION` in `.gitlab-ci.yml`.
3.  On the releases page again, view the checksums file for the latest Hugo release and note the checksum for the file `hugo_####_Linux-64bit.tar.gz`.
4.  Update the `HUGO_SHA` in `.gitlab-ci.yml`.
5.  Add, commit, and push to remote:
    ```bash
    git add .gitlab-ci.yml
    git commit -m "Updated Hugo to v####"
    git push origin master
    ```

## Updating the theme submodule
You may need to update the theme submodule if the theme needs modification for whatever reason.

At the time of writing, the currently used theme is Strata. SLOBotics maintains a fork of [digitalcraftsman/hugo-strata-theme](https://github.com/digitalcraftsman/hugo-strata-theme) on GitHub: [slobotics/hugo-strata-theme](https://github.com/slobotics/hugo-strata-theme).

After making and pushing your changes to slobotics/hugo-strata-theme on GitHub, the current version of this external repository must be updated in this repository for the changes to be incorporated into the next build of the website. Starting with a shell open to the root of the current repository, run the following commands to accomplish this.

Note that the below commands will update **all** submodules, not just the theme one. Currently the only submodule is the theme submodule. If you need to update only the theme submodule, run `git add themes/` instead of `git add .` below. Then, after pushing, run `git checkout .` and `git reset`.

```bash
# pull changes from remotes into the submodules
git submodule foreach git pull origin master
# now the submodule is in the state we want, so we can add, commit, and push to remote
git add .
# replace $reason with something like "with portfolio fixes", etc. to briefly describe what the submodule changes do
git commit -m "Updated theme submodule $reason"
git push origin master
```
