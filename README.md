# SLOBotics Website
[![build status](https://gitlab.com/slobotics/slobotics.gitlab.io/badges/master/build.svg)](https://gitlab.com/slobotics/slobotics.gitlab.io/commits/master)

The Team SLOBotics website, as seen at [slobotics.gitlab.io](https://slobotics.gitlab.io). Formerly at [slobotics.club](http://slobotics.club).

## Contributing
Please read the start of [CONTRIBUTING.md](CONTRIBUTING.md) before contributing. Note that the [CONTRIBUTING.md](CONTRIBUTING.md) file also contains guides for performing some common tasks.

## License
This project is licensed under the GNU GPLv3 license. See [LICENSE](LICENSE).
